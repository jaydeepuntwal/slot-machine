// Slot.cpp
// Author: Jaydeep Untwal, Adwait Kirtikar
// Description: A virtual slot machine game

#include <iostream>
#include <cstdlib>
#include "play.h"
using namespace std;

//
// main
//
int main() {

	cout << endl << "Welcome to Nalasopara Slot Machine!!" << endl << endl;

	int money = 0; // Amount to play game with
	int bet = 0; // Bet amount for each round
	char ch; // Character to check if user wishes to continue

	do {
		cout << "Enter the amount you wish to start playing with:" << endl;
		cin >> money;
	} while (money <= 0);

	do {
		cout << endl << "Do you wish to play ? (Y/N)" << endl;
		cin >> ch;

		if (ch == 'y' || ch == 'Y') {

			if (money > 0) {

				cout << endl << "Enter your bet:" << endl;
				cin >> bet;

				if (bet > 4) {
					cout << endl << "Bet can only range from 1 to 4" << endl;
				} else {
					money = play(money, bet); // call play for current 'money' and 'bet'
				}

			} else {
				cout << endl
						<< "Beggers are not players :P Come back when you have enough money :P"
						<< endl;
				exit(0);
			}

		} else {
			break;
		}

	} while (ch != 'n' || ch != 'N');

	cout << endl
			<< "Thank You for Playing Slot Machine... Enjoy with your money... Atleast Virtually (If you are left with any) :P"
			<< endl;

} // main
