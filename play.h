// play.h
// Author: Jaydeep Untwal, Adwait Kirtikar
// Description: Header File for Slot Machine game

#ifndef PLAY_H
#define PLAY_H

#include <iostream>
#include <cstdlib>
using namespace std;

/*
 * Name: play
 *
 * Description: play slot machine for current bet
 *
 * Arguments: 'money' - current balance, 'bet' - current bet amount
 *
 * Returns: updated 'money' according to win or lose
 *
 * Pre: 'money' should be greater than '0' and 'bet' should be less than or equal current money
 *
 * Post: 'money' is updated in both win and lose cases
 *
 */
int play(int Money, int bet);

#endif /* PLAY_H */
